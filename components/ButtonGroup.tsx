import * as React from 'react';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import { Paper, Typography } from '@mui/material';

export default function ButtonGroupComponents() {
  return (

    <Paper elevation={3} sx={{ padding: 4, flexDirection: 'column', alignItems: 'center', marginTop: 3, width: 'fit-content', textAlign: 'center' }}>
      <Typography variant="h5" gutterBottom sx={{ fontWeight: 'bold', fontFamily: 'YourFontFamily', marginBottom: 3 }}>
        ButtonGroup
      </Typography>
      <Typography variant="h6" gutterBottom sx={{ marginBottom: 4 }}>
        เป็นคอมโพนเนนต์ที่ช่วยให้จัดกลุ่มปุ่มไว้ด้วยกันในกลุ่มเดียวกัน.
      </Typography>
      <Typography variant="h6" gutterBottom sx={{ fontWeight: 'bold', marginBottom: 2, textAlign: 'left', width: '100%', marginTop: 1 }}>
        ตัวอย่าง :dfwwdds
      </Typography>

      <ButtonGroup variant="contained" aria-label="outlined primary button group" sx={{ mx: 'auto', textAlign: 'center' }}>
        <Button>หนึ่ง</Button>
        <Button>สอง</Button>
        <Button>สาม</Button>
      </ButtonGroup>

    </Paper>

  );
}