import * as React from 'react';
import Box from '@mui/material/Box';
import Rating from '@mui/material/Rating';
import Typography from '@mui/material/Typography';
import { Paper } from '@mui/material';

export default function BasicRating() {
  const [value, setValue] = React.useState<number | null>(2);

  return (

    <Paper elevation={3} sx={{ padding: 4, flexDirection: 'column', alignItems: 'center', marginTop: 3, width: 'fit-content', textAlign: 'center' }}>
      <Typography variant="h5" gutterBottom sx={{ fontWeight: 'bold', fontFamily: 'YourFontFamily', marginBottom: 3 }}>
        Rating
      </Typography>
      <Typography variant="h6" gutterBottom sx={{ marginBottom: 4 , textAlign: 'left', width: '100%'}}>
        เป็นคอมโพนเนนต์ที่ให้ผู้ใช้ทำการให้คะแนนหรือประเมินเนื้อหาต่าง ๆ ในแอปพลิเคชัน. 
        Rating มักถูกใช้ในการบอกความคิดเห็นหรือคะแนนของผู้ใช้ต่อสินค้า, บทความ, หรือข้อมูลอื่น ๆ.
      </Typography>
      <Typography variant="h6" gutterBottom sx={{ fontWeight: 'bold', marginBottom: 2, textAlign: 'left', width: '100%', marginTop: 1 }}>
        ตัวอย่าง :
      </Typography> 

    <Box
      sx={{
        '& > legend': { mt: 2 },
      }}
    >
      <Typography component="legend">Controlled2</Typography>
      <Rating
        name="simple-controlled"
        value={value}
        onChange={(event, newValue) => {
          setValue(newValue);
        }}
      />
      <Typography component="legend">Read only</Typography>
      <Rating name="read-only" value={value} readOnly />
      <Typography component="legend">Disabled</Typography>
      <Rating name="disabled" value={value} disabled />
      <Typography component="legend">No rating given</Typography>
      <Rating name="no-value" value={null} />
    </Box>

    </Paper>

  );
}