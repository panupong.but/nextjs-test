import * as React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import Divider from '@mui/material/Divider';
import { Paper, Typography } from '@mui/material';

const style = {
  width: '100%',
  maxWidth: 370,
  bgcolor: 'background.paper',
};

export default function ListDividers() {
  return (
    <Paper elevation={3} sx={{ padding: 4, flexDirection: 'column', alignItems: 'center', marginTop: 3, width: 'fit-content', textAlign: 'center' }}>
    <Typography variant="h5" gutterBottom sx={{ fontWeight: 'bold', fontFamily: 'YourFontFamily', marginBottom: 3 }}>
    Divider
    </Typography>
    <Typography variant="h6" gutterBottom sx={{ marginBottom: 4 , textAlign: 'left', width: '100%'}}>
    เป็นคอมโพนเนนต์ที่ใช้เพื่อแบ่งแยกหรือทำเส้นขีดระหว่างส่วนต่าง ๆ ใน UI. 
    Divider มักถูกใช้เพื่อสร้างเส้นแบ่งระหว่างรายการหรือส่วนต่าง ๆ เพื่อทำให้ UI ดูสะดวกและเป็นระเบียบ.
    </Typography>
    <Typography variant="h6" gutterBottom sx={{ fontWeight: 'bold', marginBottom: 2, textAlign: 'left', width: '100%', marginTop: 1 }}>
      ตัวอย่าง :
    </Typography> 
    <List sx={{ ...style, mx: 'auto', textAlign: 'center', justifyContent: 'center' }} component="nav" aria-label="mailbox folders">
      <ListItem button>
        <ListItemText primary="Inbox" />
      </ListItem>
      <Divider />
      <ListItem button divider>
        <ListItemText primary="Drafts" />
      </ListItem>
      <ListItem button>
        <ListItemText primary="Trash" />
      </ListItem>
      <Divider light />
      <ListItem button>
        <ListItemText primary="Spam" />
      </ListItem>
    </List>
    </Paper>
  );
}