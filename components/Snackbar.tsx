import * as React from 'react';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert, { AlertProps } from '@mui/material/Alert';
import { Paper, Typography } from '@mui/material';

const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
  props,
  ref,
) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

export default function CustomizedSnackbars() {
  const [open, setOpen] = React.useState(false);

  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };

  return (
    <Paper elevation={3} sx={{ padding: 4, flexDirection: 'column', alignItems: 'center', marginTop: 3, width: 'fit-content', textAlign: 'center' }}>
    <Typography variant="h5" gutterBottom sx={{ fontWeight: 'bold', fontFamily: 'YourFontFamily', marginBottom: 3 }}>
    Snackbar
    </Typography>
    <Typography variant="h6" gutterBottom sx={{ marginBottom: 4 , textAlign: 'left', width: '100%'}}>
    เป็นคอมโพนเนนต์ที่ใช้ในการแสดงข้อความหรือสถานะในด้านล่างของหน้าจอ เช่น การแจ้งเตือน, ข้อความยืนยัน, หรือข้อมูลสถานะที่ต้องการให้ผู้ใช้ทราบ.
    </Typography>
    <Typography variant="h6" gutterBottom sx={{ fontWeight: 'bold', marginBottom: 2, textAlign: 'left', width: '100%', marginTop: 1 }}>
      ตัวอย่าง :
    </Typography> 
    <Stack spacing={2} sx={{ width: '30%' , mx: 'auto', textAlign: 'center', justifyContent: 'center' }}>
      <Button variant="outlined" onClick={handleClick}>
 เปิดสำเร็จ
      </Button>
      <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
          This is a success message!
        </Alert>
      </Snackbar>
   
    </Stack>
    </Paper>
  );
}