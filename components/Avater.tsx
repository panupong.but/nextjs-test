import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import AvatarGroup from '@mui/material/AvatarGroup';
import { Paper, Typography } from '@mui/material';

export default function GroupAvatars() {
  return (
    <Paper elevation={3} sx={{ padding: 4, flexDirection: 'column', alignItems: 'center', marginTop: 3, width: 'fit-content', textAlign: 'center' }}>
    <Typography variant="h5" gutterBottom sx={{ fontWeight: 'bold', fontFamily: 'YourFontFamily', marginBottom: 3 }}>
      Avatar
    </Typography>
    <Typography variant="h6" gutterBottom sx={{ marginBottom: 4 , textAlign: 'left', width: '100%'}}>
    Avatar เป็นคอมโพนเนนต์ที่ใช้แสดงภาพโปรไฟล์หรือไอคอนตัวตนของผู้ใช้.
    Avatar มักถูกใช้ในการแสดงรูปภาพของผู้ใช้หรือตัวแทนสัญลักษณ์อื่น ๆ.
    </Typography>
    <Typography variant="h6" gutterBottom sx={{ fontWeight: 'bold', marginBottom: 2, textAlign: 'left', width: '100%', marginTop: 1 }}>
      ตัวอย่าง :
    </Typography> 
    <AvatarGroup max={4} sx={{ mx: 'auto', textAlign: 'center', justifyContent: 'center' }}>
      <Avatar alt="test 4" src="/static/images/avatar/1.jpg" />
      <Avatar alt="Travis Howard" src="/static/images/avatar/2.jpg" />
      <Avatar alt="Cindy Baker" src="/static/images/avatar/3.jpg" />
      <Avatar alt="Agnes Walker" src="/static/images/avatar/4.jpg" />
      <Avatar alt="Trevor Henderson2" src="/static/images/avatar/5.jpg" />
    </AvatarGroup>
    </Paper>
  );
}