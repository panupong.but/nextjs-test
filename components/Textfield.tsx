import * as React from 'react';
import Box from '@mui/material/Box';
import Input from '@mui/material/Input';
import InputLabel from '@mui/material/InputLabel';
import InputAdornment from '@mui/material/InputAdornment';
import FormControl from '@mui/material/FormControl';
import TextField from '@mui/material/TextField';
import AccountCircle from '@mui/icons-material/AccountCircle';
import { Paper, Typography } from '@mui/material';

export default function InputWithIcon() {
  return (
    <Paper elevation={3} sx={{ padding: 4, flexDirection: 'column', alignItems: 'center', marginTop: 3, width: 'fit-content', textAlign: 'center' }}>
    <Typography variant="h5" gutterBottom sx={{ fontWeight: 'bold', fontFamily: 'YourFontFamily', marginBottom: 3 }}>
      TextField
    </Typography>
    <Typography variant="h6" gutterBottom sx={{ marginBottom: 4 , textAlign: 'left', width: '100%'}}>
      เป็นคอมโพนเนนต์ที่ให้ผู้ใช้ป้อนข้อความหรือข้อมูลในฟอร์ม โดยมักใช้สำหรับการรับข้อมูลจากผู้ใช้ในแบบของข้อความ, อีเมล, รหัสผ่าน, หรือข้อมูลอื่น ๆ..
    </Typography>
    <Typography variant="h6" gutterBottom sx={{ fontWeight: 'bold', marginBottom: 2, textAlign: 'left', width: '100%', marginTop: 1 }}>
      ตัวอย่าง :
    </Typography> 

    <Box sx={{ '& > :not(style)': { m: 1 } ,mx: 'auto', textAlign: 'center', justifyContent: 'center', display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
      <FormControl variant="standard">
        <InputLabel htmlFor="input-with-icon-adornment">
          With a start Home
        </InputLabel>
        <Input
          id="input-with-icon-adornment"
          startAdornment={
            <InputAdornment position="start">
              <AccountCircle />
            </InputAdornment>
          }
        />
      </FormControl>
      <TextField
        id="input-with-icon-textfield"
        label="TextField"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <AccountCircle />
            </InputAdornment>
          ),
        }}
        variant="standard"
      />
      <Box sx={{ display: 'flex', alignItems: 'flex-end' }}>
        <AccountCircle sx={{ color: 'action.active', mr: 1, my: 0.5 }} />
        <TextField id="input-with-sx" label="With sx" variant="standard" />
      </Box>
    </Box>

    </Paper>
  );
}