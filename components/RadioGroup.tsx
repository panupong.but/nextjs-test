import * as React from 'react';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import { Paper, Typography } from '@mui/material';

export default function RadioButtonsGroup() {
  return (

    <Paper elevation={3} sx={{ padding: 4, flexDirection: 'column', alignItems: 'center', marginTop: 3, width: 'fit-content', textAlign: 'center' }}>
      <Typography variant="h5" gutterBottom sx={{ fontWeight: 'bold', fontFamily: 'YourFontFamily', marginBottom: 3 }}>
        RadioGroup
      </Typography>
      <Typography variant="h6" gutterBottom sx={{ marginBottom: 4 , textAlign: 'left', width: '100%'}}>
        RadioGroup เป็นคอมโพนเนนต์ที่ใช้สร้างกลุ่มของปุ่มวงกลม (Radio Buttons) ซึ่งให้ผู้ใช้เลือกตัวเลือกเดียวจากกลุ่มนั้น. 
        ทำให้เหมาะสำหรับกรณีที่คุณต้องการให้ผู้ใช้ทำการเลือกตัวเลือกเดียวจากหลายตัวเลือกที่เกี่ยวข้อง.
      </Typography>
      <Typography variant="h6" gutterBottom sx={{ fontWeight: 'bold', marginBottom: 2, textAlign: 'left', width: '100%', marginTop: 1 }}>
        ตัวอย่าง :
      </Typography> 

    <FormControl>
      <FormLabel id="demo-radio-buttons-group-label">Gender</FormLabel>
      <RadioGroup
        aria-labelledby="demo-radio-buttons-group-label"
        defaultValue="female"
        name="radio-buttons-group"
      >
        <FormControlLabel value="female" control={<Radio />} label="Female2" />
        <FormControlLabel value="male" control={<Radio />} label="Male" />
        <FormControlLabel value="other" control={<Radio />} label="Other" />
      </RadioGroup>
    </FormControl>

    </Paper>
  );
}