import * as React from 'react';
import FormatAlignLeftIcon from '@mui/icons-material/FormatAlignLeft';
import FormatAlignCenterIcon from '@mui/icons-material/FormatAlignCenter';
import FormatAlignRightIcon from '@mui/icons-material/FormatAlignRight';
import FormatAlignJustifyIcon from '@mui/icons-material/FormatAlignJustify';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import { Paper, Typography } from '@mui/material';

export default function ToggleButtons() {
  const [alignment, setAlignment] = React.useState<string | null>('left');

  const handleAlignment = (
    event: React.MouseEvent<HTMLElement>,
    newAlignment: string | null,
  ) => {
    setAlignment(newAlignment);
  };

  return (
    <Paper elevation={3} sx={{ padding: 4, flexDirection: 'column', alignItems: 'center', marginTop: 3, width: 'fit-content', textAlign: 'center' }}>
    <Typography variant="h5" gutterBottom sx={{ fontWeight: 'bold', fontFamily: 'YourFontFamily', marginBottom: 3 }}>
      ToggleButton
    </Typography>
    <Typography variant="h6" gutterBottom sx={{ marginBottom: 4 , textAlign: 'left', width: '100%'}}>
      เป็นคอมโพนเนนต์ที่ให้ผู้ใช้ทำการเปลี่ยนสถานะระหว่างเปิดและปิด หรือตัวเลือกที่สามารถเลือกหลายตัวเลือกได้. 
      ToggleButton มักถูกใช้ในกรณีที่ต้องการให้ผู้ใช้ทำการเลือกตัวเลือกที่สามารถมีสถานะได้หลายตัวเลือก.
    </Typography>
    <Typography variant="h6" gutterBottom sx={{ fontWeight: 'bold', marginBottom: 2, textAlign: 'left', width: '100%', marginTop: 1 }}>
      ตัวอย่าง :
    </Typography> 
    <ToggleButtonGroup
      value={alignment}
      exclusive
      onChange={handleAlignment}
      aria-label="text alignment"
    >
      <ToggleButton value="left" aria-label="left aligned">
        <FormatAlignLeftIcon />
      </ToggleButton>
      <ToggleButton value="center" aria-label="centered">
        <FormatAlignCenterIcon />
      </ToggleButton>
      <ToggleButton value="right" aria-label="right aligned">
        <FormatAlignRightIcon />
      </ToggleButton>
      <ToggleButton value="home" aria-label="justified" disabled>
        <FormatAlignJustifyIcon />
      </ToggleButton>
    </ToggleButtonGroup>
    </Paper>
  );
}