import * as React from 'react';
import Alert from '@mui/material/Alert';
import Stack from '@mui/material/Stack';
import { Paper, Typography } from '@mui/material';

export default function FilledAlerts() {
  
  return (
    <Paper elevation={3} sx={{ padding: 4, flexDirection: 'column', alignItems: 'center', marginTop: 3, width: 'fit-content', textAlign: 'center' }}>
    <Typography variant="h5" gutterBottom sx={{ fontWeight: 'bold', fontFamily: 'YourFontFamily', marginBottom: 3 }}>
      Alert
    </Typography>
    <Typography variant="h6" gutterBottom sx={{ marginBottom: 4 , textAlign: 'left', width: '100%'}}>
    Alert เป็นคอมโพนเนนต์ที่ใช้ในการแสดงข้อความแจ้งเตือนหรือสถานะต่าง ๆ ในแอปพลิเคชันหรือเว็บไซต์. 
    Alert มักถูกใช้เพื่อแสดงข้อความเตือนในกรณีเหตุการณ์ที่ผู้ใช้ควรทราบ, เช่น การยืนยันการทำรายการ, การแจ้งเตือนข้อผิดพลาด, หรือการแสดงข้อมูลสถานะ.
    </Typography>
    <Typography variant="h6" gutterBottom sx={{ fontWeight: 'bold', marginBottom: 2, textAlign: 'left', width: '100%', marginTop: 1 }}>
      ตัวอย่าง : ex 2
    </Typography> 

    <Stack sx={{ width: '100%' }} spacing={2}>
      <Alert variant="filled" severity="error">
        This is an error alert — check it out!
      </Alert>
      <Alert variant="filled" severity="warning">
        This is a warning alert — check it out!
      </Alert>
      <Alert variant="filled" severity="info">
        This is an info alert — check it out!
      </Alert>
      <Alert variant="filled" severity="success">
        This is a success alert — check it out!2
      </Alert>
    </Stack>
    </Paper>
  );
}