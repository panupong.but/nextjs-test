import * as React from 'react';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import { Box } from '@mui/material';

export default function ButtonComponents() {
  return (

    <Paper elevation={3} sx={{ padding: 4, flexDirection: 'column', alignItems: 'center', marginTop: 3, width: 'fit-content', textAlign: 'center' }}>
      <Typography variant="h5" gutterBottom sx={{ fontWeight: 'bold', fontFamily: 'YourFontFamily', marginBottom: 3 }}>
        Button
      </Typography>
      <Typography variant="h6" gutterBottom sx={{ marginBottom: 4 }}>
        คอมโพนเนนต์ที่ใช้สร้างปุ่มที่สามารถกดได้ในเว็บแอปพลิเคชัน.
      </Typography>
      <Typography variant="h6" gutterBottom sx={{ fontWeight: 'bold', marginBottom: 2, textAlign: 'left', width: '100%', marginTop: 1 }}>
        ตัวอย่าง :
      </Typography>

      <Stack spacing={2} direction="row" sx={{ mx: 'auto', textAlign: 'center', justifyContent: 'center' }}>
        <Button variant="text" sx={{ color: 'green' }}>
          ข้อความในปุ่ม
        </Button>
        <Button variant="contained">Contained</Button>
        <Button variant="outlined">Outlined2</Button>
      </Stack>

    </Paper>

  );
}
