import Head from 'next/head'
import styles from '@/styles/Home.module.css'
import ComboBox from '@/components/Combo-box'
import ButtonComponents from '@/components/Buttton'
import ButtonGroupComponents from '@/components/ButtonGroup'
import CustomizedSnackbars from '@/components/Snackbar'
import CheckboxesGroup from '@/components/CheckBoxGroup'
import FloatingActionButtons from '@/components/FloatingButton'
import RadioButtonsGroup from '@/components/RadioGroup'
import BasicRating from '@/components/Rating'
import SelectOtherProps from '@/components/Seletc'
import Facebook from '@/components/Skeleton'
import DiscreteSliderLabel from '@/components/Slider'
import CustomizedSwitches from '@/components/Switch'
import CircularColor from '@/components/Progress'
import InputWithIcon from '@/components/Textfield'
import ScrollDialog from '@/components/Basic-dialog'
import TransferList from '@/components/Transferlist'
import SimpleBackdrop from '@/components/Backdrop'
import FilledAlerts from '@/components/Alert'
import Types from '@/components/Typography'
import CustomizedTooltips from '@/components/Tooltip'
import DataTable from '@/components/Table'
import ToggleButtons from '@/components/ToggleButton'
import NestedList from '@/components/Lists'
import GroupAvatars from '@/components/Avater'

import FontAwesomeSvgIconDemo from '@/components/Icon'
import ListDividers from '@/components/Divider'
import Link from 'next/link'
import BadgeVisibility from '@/components/Badge'
import ClickableAndDeletableChips from '@/components/Chip'
import MaterialIcons from '@/components/Material-Icons'
import CustomizedAccordions from '@/components/Accordion'
import PrimarySearchAppBar from '@/components/App-Bar'
import RecipeReviewCard from '@/components/Card'
import Elevation from '@/components/Paper'


export default function Home() {
  return (
    <>
    <Link href="/home">home</Link>
      <Head>
        <title>Test Next.JS MUI5</title>
        <meta name="description" content="Generated by create next app" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
        <main className={styles.main}>
          <ComboBox/>
          <ButtonComponents/>
          <ButtonGroupComponents/>
          <CheckboxesGroup/>
          <FloatingActionButtons/>
          <RadioButtonsGroup/>
          <BasicRating/>
          <SelectOtherProps/>
          <DiscreteSliderLabel/>
          <CustomizedSwitches/>
          <InputWithIcon/>
          <TransferList/>
          <ToggleButtons/>
          <GroupAvatars/>
          <BadgeVisibility/>
          <ClickableAndDeletableChips/>
          <ListDividers/>
          <FontAwesomeSvgIconDemo/>
          <MaterialIcons/>
          <NestedList/>
          <DataTable/>
          <CustomizedTooltips/>
          <Types/>
          <FilledAlerts/>
          <SimpleBackdrop/>
          <ScrollDialog/>
          <CircularColor/>
          <Facebook/>
          <CustomizedSnackbars/>
          <CustomizedAccordions/>
          <PrimarySearchAppBar/>
          <RecipeReviewCard/>
          <Elevation/>
          <div>Main <input></input>
          <label>1</label>
          <label>commit1</label>
          <label>commit2</label>
          <label>commit3</label>
          <label>commit4</label>
          <label>commit5</label>

          <label>commit6</label>
          <label>commit7</label>
          <label>commit8</label>
          <label>commit9</label>
          <label>commit10</label>
          </div>
        </main>
     </>
  )
}
