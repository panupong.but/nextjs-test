import * as React from 'react';
import Stack from '@mui/material/Stack';
import CircularProgress from '@mui/material/CircularProgress';
import { Paper, Typography } from '@mui/material';

export default function CircularColor() {
  return (
    <Paper elevation={3} sx={{ padding: 4, flexDirection: 'column', alignItems: 'center', marginTop: 3, width: 'fit-content', textAlign: 'center' }}>
    <Typography variant="h5" gutterBottom sx={{ fontWeight: 'bold', fontFamily: 'YourFontFamily', marginBottom: 3 }}>
    Progress
    </Typography>
    <Typography variant="h6" gutterBottom sx={{ marginBottom: 4 , textAlign: 'left', width: '100%'}}>
    Progress เป็นคอมโพนเนนต์ที่ใช้ในการแสดงแถบความคืบหน้า (progress bar) เพื่อแสดงสถานะการดำเนินการหรือโหลดข้อมูล. 
    Progress มีหลายลูกเล่นที่คุณสามารถเลือกใช้ตามความต้องการของแอปพลิเคชันหรือเว็บไซต์ของคุณ.
    </Typography>
    <Typography variant="h6" gutterBottom sx={{ fontWeight: 'bold', marginBottom: 2, textAlign: 'left', width: '100%', marginTop: 1 }}>
      ตัวอย่าง :asda
    </Typography> 
    <Stack sx={{ color: 'grey.520',mx: 'auto', textAlign: 'center', justifyContent: 'center' }} spacing={2} direction="row">
      <CircularProgress color="secondary" />
      <CircularProgress color="success" />
      <CircularProgress color="inherit" />
    </Stack>
    </Paper>
  );
}