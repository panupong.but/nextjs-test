import * as React from 'react';
import Box from '@mui/material/Box';
import Fab from '@mui/material/Fab';
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import FavoriteIcon from '@mui/icons-material/Favorite';
import NavigationIcon from '@mui/icons-material/Navigation';
import { Paper, Typography } from '@mui/material';

export default function FloatingActionButtons() {
  return (
    <Paper elevation={3} sx={{ padding: 4, flexDirection: 'column', alignItems: 'center', marginTop: 3, width: 'fit-content', textAlign: 'center' }}>
      <Typography variant="h5" gutterBottom sx={{ fontWeight: 'bold', fontFamily: 'YourFontFamily', marginBottom: 3 }}>
        Floating Action Button (FAB)
      </Typography>
      <Typography variant="h6" gutterBottom sx={{ marginBottom: 4 , textAlign: 'left', width: '100%'}}>
        คอมโพนเนนต์ที่แสดงปุ่มกระทำสำคัญที่สร้างเนื้อหาหลักในแอปพลิเคชัน โดย FAB มักจะตั้งอยู่ด้านล่างขวาของหน้าจอ 
        และมีรูปแบบวงกลมโดยทั่วไป.
        Floating Action Button มักถูกใช้สำหรับการกระทำที่เกี่ยวข้องกับเนื้อหาหลัก
        หรือการทำงานที่ในแอปพลิเคชัน.
      </Typography>
      <Typography variant="h6" gutterBottom sx={{ fontWeight: 'bold', marginBottom: 2, textAlign: 'left', width: '100%', marginTop: 1 }}>
        ตัวอย่าง :กกก
      </Typography> 

    <Box sx={{ '& > :not(style)': { m: 1 } }}>
      <Fab color="primary" aria-label="add">
        <AddIcon />
      </Fab>
      <Fab color="secondary" aria-label="edit">
        <EditIcon />
      </Fab>
      <Fab variant="extended">
        <NavigationIcon sx={{ mr: 1 }} />
        Navigate2
      </Fab>
      <Fab disabled aria-label="like">
        <FavoriteIcon />
      </Fab>
    </Box>

    </Paper>
  );
}