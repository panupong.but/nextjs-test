import * as React from 'react';
import Chip from '@mui/material/Chip';
import Stack from '@mui/material/Stack';
import { Paper, Typography } from '@mui/material';


export default function ClickableAndDeletableChips() {
  const handleClick = () => {
    console.info('You clicked the Chip.');
  };

  const handleDelete = () => {
    console.info('You clicked the delete icon.');
  };

  return (
    <Paper elevation={3} sx={{ padding: 4, flexDirection: 'column', alignItems: 'center', marginTop: 3, width: 'fit-content', textAlign: 'center' }}>
    <Typography variant="h5" gutterBottom sx={{ fontWeight: 'bold', fontFamily: 'YourFontFamily', marginBottom: 3 }}>
    Chip
    </Typography>
    <Typography variant="h6" gutterBottom sx={{ marginBottom: 4 , textAlign: 'left', width: '100%'}}>
    Chip เป็นคอมโพนเนนต์ที่ใช้แสดงป้ายกำกับ (label) หรือตัวแทนสัญลักษณ์ที่สามารถคลิกได้. 
    Chip มักถูกใช้ในการแสดงป้ายกำกับหรือตัวแทนสัญลักษณ์ของข้อมูลที่สำคัญ.
    </Typography>
    <Typography variant="h6" gutterBottom sx={{ fontWeight: 'bold', marginBottom: 2, textAlign: 'left', width: '100%', marginTop: 1 }}>
      ตัวอย่าง :
    </Typography> 
    
    <Stack direction="row" spacing={1} sx={{ mx: 'auto', textAlign: 'center', justifyContent: 'center' }}>
      <Chip
        label="Clickable Deletable2"
        onClick={handleClick}
        onDelete={handleDelete}
      />
      <Chip
        label="Clickable Deletable"
        variant="outlined"
        onClick={handleClick}
        onDelete={handleDelete}
      />
    </Stack>
    </Paper>
  );
}