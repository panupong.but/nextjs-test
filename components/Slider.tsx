import * as React from 'react';
import Box from '@mui/material/Box';
import Slider from '@mui/material/Slider';
import { Paper, Typography } from '@mui/material';

const marks = [
  {
    value: 0,
    label: '00°C',
  },
  {
    value: 20,
    label: '200°C',
  },
  {
    value: 37,
    label: '370°C',
  },
  {
    value: 100,
    label: '1000°C',
  },
];

function valuetext(value: number) {
  return `${value}°C`;
}

export default function DiscreteSliderLabel() {
  return (

    <Paper elevation={3} sx={{ padding: 4, flexDirection: 'column', alignItems: 'center', marginTop: 3, width: 'fit-content', textAlign: 'center' }}>
      <Typography variant="h5" gutterBottom sx={{ fontWeight: 'bold', fontFamily: 'YourFontFamily', marginBottom: 3 }}>
        Slider
      </Typography>
      <Typography variant="h6" gutterBottom sx={{ marginBottom: 4 , textAlign: 'left', width: '100%'}}>
      เป็นคอมโพนเนนต์ที่ให้ผู้ใช้ทำการเลื่อนเพื่อเลือกค่าตั้งแต่ช่วงที่กำหนดไว้. 
      Slider มักถูกใช้เพื่อให้ผู้ใช้เลือกค่าตัวเลขหรือปรับค่าของตัวแปรต่าง ๆ ที่มีค่าอยู่ในช่วงที่กำหนด.
      </Typography>
      <Typography variant="h6" gutterBottom sx={{ fontWeight: 'bold', marginBottom: 2, textAlign: 'left', width: '100%', marginTop: 1 }}>
        ตัวอย่าง :
      </Typography> 

    <Box sx={{ width: 300 , mx: 'auto', textAlign: 'center', justifyContent: 'center' }}>
      <Slider
        aria-label="Always visible3"
        defaultValue={80}
        getAriaValueText={valuetext}
        step={1}
        marks={marks}
        valueLabelDisplay="on"
      />
    </Box>

  </Paper>

  );
}