import * as React from 'react';
import Stack from '@mui/material/Stack';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import CameraAltOutlinedIcon from '@mui/icons-material/CameraAltOutlined';

export default function MaterialIcons() {
  return (

    <Paper elevation={3} sx={{ padding: 4, flexDirection: 'column', alignItems: 'center', marginTop: 3, width: 'fit-content', textAlign: 'center' }}>
      <Typography variant="h5" gutterBottom sx={{ fontWeight: 'bold', fontFamily: 'YourFontFamily', marginBottom: 3 }}>
      Material Icons
      </Typography>
      <Typography variant="h6" gutterBottom sx={{ marginBottom: 4 }}>
      ไอคอนที่เกี่ยวข้องกับ Material Design Icons สามารถใช้ได้โดยตรงจากไลบรารีของ Material-UI โดยไม่ต้องนำเข้าเพิ่ม.
      </Typography>
      <Typography variant="h6" gutterBottom sx={{ fontWeight: 'bold', marginBottom: 2, textAlign: 'left', width: '100%', marginTop: 1 }}>
        ตัวอย่าง : fdl
      </Typography>

      <Stack spacing={2} direction="row" sx={{ mx: 'auto', textAlign: 'center', justifyContent: 'center' }}>
        <CameraAltOutlinedIcon/>
      </Stack>
    </Paper>
       
  );
}
