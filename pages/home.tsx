import { Box, Button, Typography } from '@mui/material';

const Home: React.FC = () => {
  return (
    <Box
      sx={{
        backgroundColor: '#f0f0f0', // สีพื้นหลัง
        padding: 2, // ระยะห่าง
        border: 2,
        margin:5,
      }}
    >
      <Typography variant="h1" color="primary" gutterBottom>
        use Git rebase and commit 
        code
      </Typography>
      <Button variant="contained" color="error">
        Click me
      </Button>
    </Box>
  );
};

export default Home;
